<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Stringst Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'Application' => 'Application',
    'Assignment' => 'Assignment',
    'Dashboard' => 'Dashboard',
    'assignment _overview'=> 'Assignment Overview',
    'Statistics'=> 'Statistics'
];
